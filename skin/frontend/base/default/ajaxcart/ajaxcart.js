var ajaxcart = {
    g: new Growler(),
    initialize: function() {
        this.g = new Growler({
            width: '100%'
        });
        this.bindEvents();
    },
    bindEvents: function () {
        this.addSubmitEvent();

        $$('a[href*="/checkout/cart/delete/"]').each(function(e){
            $(e).observe('click', function(event){
                setLocation($(e).readAttribute('href'));
                Event.stop(event);
            });
        });
    },
    ajaxCartSubmit: function (obj) {
        var _this = this;
        if(Modalbox !== 'undefined' && Modalbox.initialized)Modalbox.hide();

        try {
            if(typeof obj == 'string') {
                var url = obj;

                new Ajax.Request(url, {
                    onCreate	: function() {
                        _this.g.warn("Processing...", {
                            life: 1,
                            header: 'Wait a second, please!'
                        });
                    },
                    onFailure: function(failedResponse) {
                        console.log('Request failed');
                        console.log(failedResponse);
                    },
                    onSuccess	: function(response) {
                        // Handle the response content...
                        try{
                            var res = response.responseText.evalJSON();
                            if(res) {
                                //check for group product's option
                                if(res.configurable_options_block) {
                                    if(res.r == 'success') {
                                        //show group product options block
                                        _this.showPopup(res.configurable_options_block);
                                    } else {
                                        if(typeof res.messages != 'undefined') {
                                            _this.showError(res.messages);
                                        } else {
                                            _this.showError("Something bad happened");
                                        }
                                    }
                                } else {
                                    if(res.r == 'success') {
                                        if(res.message) {
                                            _this.showSuccess(res.message);
                                        } else {
                                            _this.showSuccess('Item was added into cart.');
                                        }

                                        //update all blocks here
                                        _this.updateBlocks(res.update_blocks);

                                    } else {
                                        if(typeof res.messages != 'undefined') {
                                            _this.showError(res.messages);
                                        } else {
                                            _this.showError("Something bad happened");
                                        }
                                    }
                                }
                            } else {
                                document.location.reload(true);
                            }
                        } catch(e) {
                        //window.location.href = url;
                        //document.location.reload(true);
                        }
                    }
                });
            } else {
                if(typeof obj.form.down('input[type=file]') != 'undefined') {

                    //use iframe

                    obj.form.insert('<iframe id="upload_target" name="upload_target" src="" style="width:0;height:0;border:0px solid #fff;"></iframe>');

                    var iframe = $('upload_target');
                    iframe.observe('load', function(){
                        // Handle the response content...
                        try{
                            var doc = iframe.contentDocument ? iframe.contentDocument : (iframe.contentWindow.document || iframe.document);
                            console.log(doc);
                            var res = doc.body.innerText ? doc.body.innerText : doc.body.textContent;
                            res = res.evalJSON();

                            if(res) {
                                if(res.r == 'success') {
                                    if(res.message) {
                                        _this.showSuccess(res.message);
                                    } else {
                                        _this.showSuccess('Item was added into cart.');
                                    }

                                    //update all blocks here
                                    _this.updateBlocks(res.update_blocks);

                                } else {
                                    if(typeof res.messages != 'undefined') {
                                        _this.showError(res.messages);
                                    } else {
                                        _this.showError("Something bad happened");
                                    }
                                }
                            } else {
                                _this.showError("Something bad happened");
                            }
                        } catch(e) {
                            console.log(e);
                            _this.showError("Something bad happened");
                        }
                    });

                    obj.form.target = 'upload_target';

                    //show loading
                    _this.g.warn("Processing", {
                        life: 5
                    });

                    obj.form.submit();
                    return true;

                } else {
                    //use ajax

                    var url	 = 	obj.form.action,
                    data =	obj.form.serialize();

                    new Ajax.Request(url, {
                        method		: 'post',
                        postBody	: data,
                        onCreate	: function() {
                            _this.g.warn("Processing", {
                                life: 5
                            });
                        },
                        onSuccess	: function(response) {
                            // Handle the response content...
                            try{
                                var res = response.responseText.evalJSON();

                                if(res) {
                                    if(res.r == 'success') {
                                        if(res.message) {
                                            _this.showSuccess(res.message);
                                        } else {
                                            _this.showSuccess('Item was added into cart.');
                                        }

                                        //update all blocks here
                                        _this.updateBlocks(res.update_blocks);

                                    } else {
                                        if(typeof res.messages != 'undefined') {
                                            _this.showError(res.messages);
                                        } else {
                                            _this.showError("Something bad happened");
                                        }
                                    }
                                } else {
                                    _this.showError("Something bad happened");
                                }
                            } catch(e) {
                                console.log(e);
                                _this.showError("Something bad happened");
                            }
                        }
                    });
                }
            }
        } catch(e) {
            console.log(e);
            if(typeof obj == 'string') {
                window.location.href = obj;
            } else {
                document.location.reload(true);
            }
        }
    },
    
    getConfigurableOptions: function(url) {
        var _this = this;
        new Ajax.Request(url, {
            onCreate	: function() {
                _this.g.warn("Processing", {
                    life: 5
                });
            },
            onSuccess	: function(response) {
                // Handle the response content...
                try{
                    var res = response.responseText.evalJSON();
                    if(res) {
                        if(res.r == 'success') {
                            
                            //show configurable options popup
                            _this.showPopup(res.configurable_options_block);

                        } else {
                            if(typeof res.messages != 'undefined') {
                                _this.showError(res.messages);
                            } else {
                                _this.showError("Something bad happened");
                            }
                        }
                    } else {
                        document.location.reload(true);
                    }
                } catch(e) {
                window.location.href = url;
                //document.location.reload(true);
                }
            }
        });
    },

    showSuccess: function(message) {
        this.g.info(message, {
            life: 5
        });
    },

    showError: function (error) {
        var _this = this;

        if(typeof error == 'string') {
            _this.g.error(error, {
                life: 5
            });
        } else {
            error.each(function(message){
                _this.g.error(message, {
                    life: 5
                });
            });
        }
    },

    addSubmitEvent: function () {

        if(typeof productAddToCartForm != 'undefined') {
            var _this = this;
            productAddToCartForm.submit = function(url){
                if(this.validator && this.validator.validate()){
                    _this.ajaxCartSubmit(this);
                }
                return false;
            }

            productAddToCartForm.form.onsubmit = function() {
                productAddToCartForm.submit();
                return false;
            };
        }
    },

    updateBlocks: function(blocks) {
        var _this = this;

        if(blocks) {
            try{
                blocks.each(function(block){
                    if(block.key) {
                        var dom_selector = block.key;
                        if($$(dom_selector)) {
                            $$(dom_selector).each(function(e){
                                $(e).replace(block.value);
                            });
                        }
                    }
                });
                _this.bindEvents();

                // show details tooltip
                truncateOptions();
            } catch(e) {
                console.log(e);
            }
        }

    },
    
    showPopup: function(block) {
        try {
            var _this = this;
            //$$('body')[0].insert({bottom: new Element('div', {id: 'modalboxOptions'}).update(block)});
            var element = new Element('div', {
                id: 'modalboxOptions'
            }).update(block);
            
            var viewport = document.viewport.getDimensions();
            Modalbox.show(element,
            {
                title: 'Please Select Options', 
                width: 510,
                height: viewport.height,
                afterLoad: function() {
                    _this.extractScripts(block);
                    _this.bindEvents();
                }
            });
        } catch(e) {
            console.log(e)
        }
    },
    
    extractScripts: function(strings) {
        var scripts = strings.extractScripts();
        scripts.each(function(script){
            try {
                eval(script.replace(/var /gi, ""));
            }
            catch(e){
                console.log(e);
            }
        });
    },


    getItemQtyByProductId: function (product_id) {
        var item_cart_qty = null;
        $$('@[id*="product_' + product_id + '"]').each(function(e) {
            var regexp = /qty_([0-9]*)/g;
            m = regexp.exec($(e).getAttribute('id'));
            item_cart_qty = parseInt(m[1]);
            throw $break;
        });
        return item_cart_qty;
    },

    getItemIdByProductId: function (product_id) {
        var item_cart_id = null;
        $$('@[id*="product_' + product_id + '"]').each(function(e) {
            var regexp = /item_([0-9]*)/g;
            m = regexp.exec($(e).getAttribute('id'));
            item_cart_id = parseInt(m[1]);
            throw $break;
        });
        return item_cart_id;
    },

    getActiveBoxQty: function() {
        var box_qty = null;

        $$('#recipe-box-switcher label').each(function (e) {

            if ($(e).hasClassName('active')) {
                box_qty = $(e).select('input').first().getAttribute('value');
            }

        });

        return box_qty;

    }

};

var oldSetLocation = setLocation;
var setLocation = (function() {
    return function(url){
        if( url.search('checkout/cart/add') != -1 ) {
            //its simple/group/downloadable product
            ajaxcart.ajaxCartSubmit(url);

        //i've added
        } else if( url.search('checkout/cart/updateItemOptions') != -1 ) {
            ajaxcart.ajaxCartSubmit(url);

        }
        //updateItemsBoxes
        else if (url.search('checkout/cart/updateItemsBoxes') != -1) {
            ajaxcart.ajaxCartSubmit(url);

        }
        else if( url.search('checkout/cart/delete') != -1 ) {
            ajaxcart.ajaxCartSubmit(url);
        } else if( url.search('options=cart') != -1 ) {
            //its configurable/bundle product
            url += '&ajax=true';
            ajaxcart.getConfigurableOptions(url);
        } else {
            oldSetLocation(url);
        }
    };
})();

setPLocation = setLocation;

document.observe("dom:loaded", function() {
    ajaxcart.initialize();
});


function setLocationCustom(url, product_id) {

    var item_id = ajaxcart.getItemIdByProductId(product_id);
    var qty = ajaxcart.getItemQtyByProductId(product_id);

    var onclick_was = url;
    var onclick_new = onclick_was;

    /* changing action url*/
    if (item_id) {
        if (url.search('checkout/cart/add') != -1) {
            onclick_new  = onclick_was
                .replace('checkout/cart/add', 'checkout/cart/updateItemOptions')
                .replace('product/' + product_id,'id/' + item_id)
        }else if(url.search('checkout/cart/updateItemOptions') != -1 && !qty) {
            onclick_new  = onclick_was
                .replace('checkout/cart/updateItemOptions', 'checkout/cart/add')
                .replace('id/' + item_id,'product/' + product_id)
        }

    }


    /* seeing for a qty input and qty that was set previously */
    var qty_button = $$('#item-buttons-'+ product_id +' input.item-qty').first();
    var input_qty = parseInt(qty_button.getValue());

    var sum_qty = isNaN(parseInt(qty)) ? 0 : parseInt(qty);
    sum_qty = sum_qty + (isNaN(parseInt(input_qty)) ? 1 : parseInt(input_qty));

    if (sum_qty > 0) {
        //replacing new qty
        if (onclick_new.search('qty/') != -1) {
            console.log('here');
            onclick_new = onclick_new.replace( /(qty\/)([0-9]+)/g, "$1" + sum_qty);
        } else {
            onclick_new = onclick_new + 'qty/' + sum_qty;
        }


    }else { // if number gets below or equal zero - it's seems we should remove an item
        onclick_new  = onclick_new
            .replace('checkout/cart/add', 'checkout/cart/delete')
            .replace('checkout/cart/updateItemOptions', 'checkout/cart/delete');
    }


    console.log('action url changed to:' + onclick_new);

    setLocation(onclick_new);

}


function setLocationCustom2(url, el) {

    var product_id = $(el).up().getAttribute('id').match(/\d+$/)[0];

    //return;
    var item_id = ajaxcart.getItemIdByProductId(product_id);
    var qty = ajaxcart.getItemQtyByProductId(product_id);
    var box_qty = ajaxcart.getActiveBoxQty();

    var onclick_was = url;
    var onclick_new = onclick_was;

    /* changing action url*/
    if (item_id) {
        if (url.search('checkout/cart/add') != -1) {
            onclick_new  = onclick_was
                .replace('checkout/cart/add', 'checkout/cart/updateItemOptions')
                .replace('product/' + product_id,'id/' + item_id)
        }else if(url.search('checkout/cart/updateItemOptions') != -1 && !qty) {
            onclick_new  = onclick_was
                .replace('checkout/cart/updateItemOptions', 'checkout/cart/add')
                .replace('id/' + item_id,'product/' + product_id)
        }

    }


    /* seeing for a qty input and qty that was set previously */
//    var qty_button = $$('#item-buttons-'+ product_id +' input.item-qty').first();
//    var input_qty = parseInt(qty_button.getValue());

    var input_qty = 0;
    if ($(el).hasClassName('plus')) {
        input_qty = 1; // ++
    }else if($(el).hasClassName('minus')){
        input_qty = -1; // --
    }

    var sum_qty = isNaN(parseInt(qty)) ? 0 : parseInt(qty);
    sum_qty = sum_qty + (isNaN(parseInt(input_qty)) ? 1 : parseInt(input_qty));

    if (sum_qty > 0) {
        //replacing new qty
        if (onclick_new.search('qty/') != -1) {
            console.log('here');
            onclick_new = onclick_new.replace( /(qty\/)([0-9]+)/g, "$1" + sum_qty);
        } else {
            onclick_new = onclick_new + 'qty/' + sum_qty;
        }

        //replacing box_qty
        if (onclick_new.search('recipe_box/') != -1) {
            onclick_new = onclick_new.replace( /(recipe_box\/)([0-9]+)/g, "$1" + box_qty);
        } else {
            onclick_new = onclick_new + 'recipe_box/' + box_qty;
        }


    }else { // if number gets below or equal zero - it's seems we should remove an item
        onclick_new  = onclick_new
            .replace('checkout/cart/add', 'checkout/cart/delete')
            .replace('checkout/cart/updateItemOptions', 'checkout/cart/delete');
    }


    console.log('action url changed to:' + onclick_new);

    setLocation(onclick_new);

}