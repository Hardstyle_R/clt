<?php


class CLT_Catalog_Block_Product_View extends Mage_Catalog_Block_Product_View {

    /**
     *
     */
    public function getProductDefaultQty($product = null)
    {
        /**
         * @todo CLT Params passed here!
         */
        $boxQty = $this->getRequest()->getParam('box_qty', 20);

        $cltCookies = Mage::helper('core')->jsonDecode(Mage::getModel('core/cookie')->get('clt'));
//var_dump($cltCookies);
        if (isset($cltCookies['box_qty'])) {
            $boxQty = (int)$cltCookies['box_qty'];
        }

        if(isset($boxQty)) {
            return (int)$boxQty;
        }

        if (!$product) {
            $product = $this->getProduct();
        }

        $qty = $this->getMinimalQty($product);
        $config = $product->getPreconfiguredValues();
        $configQty = $config->getQty();
        if ($configQty > $qty) {
            $qty = $configQty;
        }

        return $qty;
    }

}