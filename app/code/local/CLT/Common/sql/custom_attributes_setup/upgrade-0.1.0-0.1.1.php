<?php

$installer = new Mage_Sales_Model_Resource_Setup('core_setup');
/**
 * Add 'custom_attribute' attribute for entities
 */
$entities = array( // Note. Exclude entities that you don’t need.
    'quote',
    'quote_address',
    'quote_item',
    'quote_address_item',
    'order',
    'order_item'
);
$options = array(
    'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible' => true,
    'required' => false
);
foreach ($entities as $entity) {
    $installer->addAttribute($entity, 'recipe_box_qty', $options);
}
$installer->endSetup();