<?php

/**
 * @var $this Mage_Catalog_Model_Resource_Setup
 */
$installer = $this;

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'recipe_box_qty', array(
    'group'             => 'General',
    'type'              => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'attribute_set' =>  'Recipe', // Your custom Attribute set
    'backend'           => '',
    'frontend'          => '',
    'label'             => 'Recipe Box Qty',
    'input'             => 'text',
    'class'             => '',
    'source'            => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => '2',
    'searchable'        => true,
    'filterable'        => true,
    'comparable'        => true,
    'visible_on_front'  => true,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,virtual',
    'is_configurable'   => true,
    'used_in_product_listing' => true
));