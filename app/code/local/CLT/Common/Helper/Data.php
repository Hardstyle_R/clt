<?php
class CLT_Common_Helper_Data extends Mage_Core_Helper_Url {

    /**
     * Return helper instance
     *
     * @param  string $helperName
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelperInstance($helperName)
    {
        return Mage::helper($helperName);
    }

    public function getCartItemUpdateUrl($item, $additional = array())
    {
        $routeParams = array(
            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->_getHelperInstance('core')
                ->urlEncode($this->getCurrentUrl()),
            'id' => $item->getId(),
            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
        );

        if (!empty($additional)) {
            $routeParams = array_merge($routeParams, $additional, array('isAjax' => true));
        }

//        if ($item->hasUrlDataObject()) {
//            $routeParams['_store'] = $item->getUrlDataObject()->getStoreId();
//            $routeParams['_store_to_url'] = true;
//        }

        if ($this->_getRequest()->getRouteName() == 'checkout'
            && $this->_getRequest()->getControllerName() == 'cart') {
            $routeParams['in_cart'] = 1;
        }

        return $this->_getUrl('checkout/cart/updateItemOptions', $routeParams);
    }


    public function getCartItemsBoxesUpdateUrl($additional = array())
    {
        $routeParams = array(
            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->_getHelperInstance('core')
                ->urlEncode($this->getCurrentUrl()),
            'id' => 0,
            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
        );

        if (!empty($additional)) {
            $routeParams = array_merge($routeParams, $additional, array('isAjax' => true, 'in_cart' => true));
        }

        return $this->_getUrl('checkout/cart/updateItemsBoxes', $routeParams);
    }


}