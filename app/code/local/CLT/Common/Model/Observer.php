<?php


class CLT_Common_Model_Observer {


    /**
     * Copying custom attributes from the product to quote item
     * @param $observer Varien_Event_Observer
     * @return $this
     */
    public function salesQuoteItemSetCustomAttribute($observer)
    {
        $item = $observer->getQuoteItem();
        /**
         * @var $loadedItem Mage_Sales_Model_Quote_Item
         */
        $product = $observer->getProduct();

        $params = Mage::app()->getRequest()->getParams();
        if (isset($params['recipe_box'])) {
            $params['recipe_box_qty'] = $params['recipe_box'];
        }

        $recipeBoxQty = (isset($params['recipe_box'])) ? $params['recipe_box'] : (isset($params['recipe_box_qty']) ? $params['recipe_box_qty'] : null);

        if ($recipeBoxQty) {
            $product->setRecipeBoxQty($recipeBoxQty);
            $item->setRecipeBoxQty($recipeBoxQty);

        }else {
            if (!$item->getRecipeBoxQty()) {
                //default from configured product
                $item->setRecipeBoxQty($product->getRecipeBoxQty()); // 'recipe_box_qty' attribute
            }
        }

//        $quote = Mage::getSingleton('checkout/cart');
//        $quote->save();

        return $this;

    }


    public function onItemCompleteAddRecipeBoxQty($observer) {

        $item = $observer->getItem();

        $cart = Mage::getSingleton('checkout/cart');

        /**
         * @var $cart Mage_Checkout_Model_Cart
         */

        $realItem = $cart->getQuote()->getItemById($item->getId());

        $params = $observer->getRequest()->getParams();

        if (isset($params['recipe_box'])) {
            $params['recipe_box_qty'] = $params['recipe_box'];
            //$realItem->setRecipeBoxQtyNeedToUpdate($params['recipe_box_qty']);
            //$realItem->setData('recipe_box_qty_needs_update', $params['recipe_box_qty']);

            $item->addOption(array(
                "product_id" => $realItem->getProduct()->getId(),
                "product" => $realItem->getProduct(),
                "code" => 'recipe_box_qty_needs_update',
                "value" => $params['recipe_box_qty']
            ));
            $item->save();
        }

        return $this;

    }


    public function AddCustomProductTypeOnUpdate(Varien_Event_Observer $observer) {

        Mage::log('AddCustomProductTypeOnUpdate trigerred');

        try {

            $quoteItem = $observer->getEvent()->getItem();
            $product = $quoteItem->getProduct();

            if (in_array($product->getId(), array(169,170,171))) {
                $option = Mage::getModel('sales/quote_item_option')
                    ->setProductId($product->getId())
                    ->setCode('product_type')
                    ->setProduct($product) // needed for EE only ?
                    ->setValue('unique_recipe');

                $quoteItem->addOption($option);
                $quoteItem->save();
            }

        }
        catch (Exception $e) {
            // log any issues, but allow system to continue.
            Mage::log($e->getMessage());
        }

        return $this;

    }

    public function AddCustomProductTypeOnAdd(Varien_Event_Observer $observer) {

        Mage::log('AddCustomProductTypeOnAdd triggered');

        try {

            $product = $observer->getEvent()->getProduct();

            $quoteItem = $observer->getEvent()->getQuoteItem();

            if (in_array($product->getId(), array(169,170,171))) {
                $option = Mage::getModel('sales/quote_item_option')
                    ->setProductId($product->getId())
                    ->setCode('product_type')
                    ->setProduct($product) // needed for EE only ?
                    ->setValue('unique_recipe');

                $quoteItem->addOption($option);
            }

        }
        catch (Exception $e) {
            // log any issues, but allow system to continue.
            Mage::log($e->getMessage());
        }

        return $this;

    }


}