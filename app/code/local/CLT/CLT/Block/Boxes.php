<?php

class CLT_CLT_Block_Boxes extends Mage_Core_Block_Template {



    public function getCatalogLink() {

        return $this->getBaseUrl() . 'week1-proposal.html';

    }

    public function setCatalogLink() {

    }

    protected static $DEFAULT_BOXES_QTY = 2;

    public function getBoxesQty() {
        if ($this->getData('box_qty')) {
            $boxQty = $this->getData('box_qty');
        }else {
            $boxQty = self::$DEFAULT_BOXES_QTY;
        }

        return $boxQty;
    }

}